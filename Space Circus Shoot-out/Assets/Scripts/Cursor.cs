﻿using UnityEngine;

public class Cursor : MonoBehaviour
{
	public const float ZPosition = -9.5f;

	private Stack ActiveStack { get; set; }

    public Sprite passive;
    public Sprite active;

	public void Connect(Stack stack)
	{
		ActiveStack = stack;
		ActiveStack.DragPositionOffset = Vector3.zero;
	}

	public void Disconnect()
	{
		if (ActiveStack != null)
		{
			if (ActiveStack.DragPositionOffset.HasValue)
			{
				var newStack = GetStackAtPosition(ActiveStack.transform.position + ActiveStack.DragPositionOffset.Value);
				if (newStack != null && ActiveStack != newStack && !newStack.IsLockdown && newStack.GetComponent<Ownership>().owner != Owner.None)
				{
					ActiveStack.TransferTopCardTo(newStack);
				}
			}

			ActiveStack.DragPositionOffset = null;
			ActiveStack = null;
		}
	}

	private bool isHeld = false;
	public bool IsHeld
	{
		get
		{
			return isHeld;
		}

		set
		{
			if (!IsHeld & value)
            {
				var stack = GetStackAtPosition(transform.position);
				if (stack != null && !stack.IsLockdown)
                {
					Connect(stack);
				}
			}

			isHeld = value;

			if (!IsHeld)
			{
				Disconnect();
			}

		    GetComponent<SpriteRenderer>().sprite = IsHeld ? active : passive;
		}
	}

	private Stack GetStackAtPosition(Vector3 position)
	{
		var colliders = Physics2D.OverlapPointAll(position);
		foreach (var collider in colliders)
        {
			if (collider != null)
            {
				var stack = collider.GetComponent<Stack>();

				if (stack != null && (GetComponent<Ownership>() == null || stack.GetComponent<Ownership>().owner == Owner.None || stack.GetComponent<Ownership>().owner == GetComponent<Ownership>().owner))
                {
					return stack;
				}
			}
		}

		return null;
	}

	void Update()
	{
		if (ActiveStack != null)
		{
			if (ActiveStack.DragPositionOffset == null)
			{
				Disconnect();
			}
			else
			{
				var offset = transform.position - ActiveStack.transform.position;
				ActiveStack.DragPositionOffset = new Vector3(offset.x, offset.y, offset.y + 0.1f);
			}
		}
	}

	public void SetPosition(Vector3 newPosition)
	{
		newPosition.z = ZPosition;
		transform.position = newPosition;
	}
}
