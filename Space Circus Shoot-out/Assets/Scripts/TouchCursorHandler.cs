﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TouchCursorHandler : MonoBehaviour
{
	private Dictionary<int, Cursor> fingerIdControls = new Dictionary<int, Cursor>();

	void Update()
	{
		for(int touchIndex = 0; touchIndex < Input.touchCount; touchIndex++)
		{
			var touch = Input.GetTouch(touchIndex);
			if (touch.phase == TouchPhase.Began)
			{
				var cursor = GameObject.Instantiate<Cursor>(GetComponent<Setup>().cursorPrefab);
				cursor.GetComponent<SpriteRenderer>().enabled = false;
				fingerIdControls.Add(touch.fingerId, cursor);
			}
			else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
			{
				fingerIdControls[touch.fingerId].IsHeld = false;
				Destroy(fingerIdControls[touch.fingerId].gameObject);
				fingerIdControls.Remove(touch.fingerId);
			}
			else
			{
				var position = touch.position;
				fingerIdControls[touch.fingerId].SetPosition(Camera.main.ScreenToWorldPoint(position));
				fingerIdControls[touch.fingerId].IsHeld = true;
			}
		}
	}
}