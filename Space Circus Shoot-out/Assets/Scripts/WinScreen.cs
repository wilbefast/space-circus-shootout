﻿using UnityEngine;
using System.Collections;

public class WinScreen : MonoBehaviour
{
	public string Text
	{
		set
		{
			GetComponent<TextMesh>().text = value;
		}
	}
}