﻿using UnityEngine;

[RequireComponent(typeof(Cursor))]
public class GamepadControl : MonoBehaviour
{
	private const float MoveSpeed = 3;

	void Update()
	{
		var owner = GetComponent<Ownership>().owner;

		var horizontal = Input.GetAxis("Horizontal" + owner);
		var vertical = -Input.GetAxis("Vertical" + owner);

		transform.position += new Vector3(horizontal, vertical) * MoveSpeed * Time.deltaTime;
        transform.position = new Vector3(transform.position.x, transform.position.y, Cursor.ZPosition);

		GetComponent<Cursor>().IsHeld = Input.GetButton("Click" + owner);
	}
}