﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Card : MonoBehaviour
{
	private Unit unitPrefab;
	public Unit UnitPrefab
	{
		get
		{
			return unitPrefab;
		}
		set
		{
			unitPrefab = value;

			UpdateCardImage();
		}
	}

	private bool isPickup = false;
	public bool IsPickup
	{
		get
		{
			return isPickup;
		}
		set
		{
			isPickup = value;
			UpdateCardImage();
		}
	}
	private void UpdateCardImage()
	{
		var setup = FindObjectOfType<Setup>();
		var prefabIndex = setup.allUnits.ToList().IndexOf(UnitPrefab);
		GetComponent<SpriteRenderer>().sprite = IsPickup ? setup.allUnitCardPickupSprites[prefabIndex] : setup.allUnitCardSprites[prefabIndex];
	}

	private Material LastMaterial { get; set; }

	public void SwapGrayscale()
	{
		var renderer = GetComponent<SpriteRenderer>();
		if (LastMaterial == null)
		{
			LastMaterial = renderer.material;
			renderer.material = Utility.GrayscaleMaterial;
		}
		else
		{
			renderer.material = LastMaterial;
			LastMaterial = null;
		}
	}
}
