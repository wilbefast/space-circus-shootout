﻿using UnityEngine;
using System.Linq;

public class Setup : MonoBehaviour
{
	public const int NumberOfStacks = 4;
	public const int NumberOfCardsPerStack = 1;

	private const float ExpectedGameLength = 3 * 60;
	private const float LongRunSpawnInterval = 4;

	public const float LaneWidth = 7.5f;
	public const float LaneHeight = 6;

	public Cursor cursorPrefab;
	public Stack stackPrefab;

	public Unit[] allUnits;
	public Sprite[] allUnitCardSprites;
	public Sprite[] allUnitCardPickupSprites;

	public AnimationCurve spawnInterval;
	public AudioSource musicPrefab;

	public AudioClip go;

	private void GoSound()
	{
		Utility.PlaySound(go);
	}

	void Start ()
	{
		Invoke("GoSound", 0.5f);

		Instantiate(musicPrefab);

		if (Input.touchSupported)
		{
			gameObject.AddComponent<TouchCursorHandler>();
		}
		else if (Input.GetJoystickNames().Length > 0 && Input.GetJoystickNames()[0] != string.Empty)
		{
			var leftCursor = Instantiate(cursorPrefab);
            leftCursor.gameObject.AddComponent<GamepadControl>().gameObject.AddComponent<Ownership>().owner = Owner.Left;
            leftCursor.GetComponent<SpriteRenderer>().material = Utility.LeftColoringMaterial;
            leftCursor.SetPosition(new Vector3(-LaneWidth / 2f, 0));

		    var rightCursor = Instantiate(cursorPrefab);
            rightCursor.gameObject.AddComponent<GamepadControl>().gameObject.AddComponent<Ownership>().owner = Owner.Right;
            rightCursor.SetPosition(new Vector3(LaneWidth / 2f, 0));
		}
		else
		{
			Instantiate(cursorPrefab).gameObject.AddComponent<MouseControl>();
		}

		var unitsToDeal = new Unit[NumberOfStacks * NumberOfCardsPerStack];
		for (int i = 0; i < unitsToDeal.Length; i++)
		{
			unitsToDeal[i] = allUnits[i % allUnits.Length];
		}

		for (int playerIndex = 0; playerIndex < 2; playerIndex++)
		{
			var playerUnitsToDeal = unitsToDeal.ToList();
			playerUnitsToDeal.Shuffle();
			for (int stackIndex = 0; stackIndex < NumberOfStacks; stackIndex++)
			{
				var stackOffset = LaneHeight / NumberOfStacks / 2;
				var y = -LaneHeight / 2 + LaneHeight / NumberOfStacks * stackIndex + stackOffset;
				var cardsToDeal = playerUnitsToDeal.Skip(stackIndex * NumberOfCardsPerStack).Take(NumberOfCardsPerStack);

				var stack = GameObject.Instantiate<Stack>(stackPrefab);
				stack.transform.position = new Vector2(-LaneWidth / 2 * (1 - playerIndex * 2), y);
				stack.GetComponent<Ownership>().owner = playerIndex == 0 ? Owner.Left : Owner.Right;
				stack.unitsInCards = cardsToDeal.ToArray();
			}
		}

		StartTimerForNextSpawn();
	}

	private void SpawnRandomCard()
	{
		Vector2? spawnPosition = new Vector2(Random.Range(0, 2), Random.Range(0, 2));

		if (GetSpawnBlocked(spawnPosition.Value))
		{
			spawnPosition = GetFirstFreeSpawnPosition();
		}

		if (spawnPosition.HasValue)
		{
			var newStack = Instantiate<Stack>(stackPrefab);
			newStack.unitsInCards = new [] { allUnits[Random.Range(0, allUnits.Length)] };
			newStack.transform.position = GetPositionForSpawnPosition(spawnPosition.Value);
		}

		StartTimerForNextSpawn();
	}

	private Vector2? GetFirstFreeSpawnPosition()
	{
		for(int y = 0; y <= 2; y++)
		{
			for(int x = 0; x <= 2; x++)
			{
				var position = new Vector2(x, y);
				if (!GetSpawnBlocked(position))
				{
					return position;
				}
			}
		}

		return null;
	}

	private bool GetSpawnBlocked(Vector2 position)
	{
		var collisions = Physics2D.OverlapPointAll(GetPositionForSpawnPosition(position));
		foreach(var collision in collisions)
		{
			if (collision.GetComponent<Stack>() != null)
			{
				return true;
			}
		}

		return false;
	}

	private Vector2 GetPositionForSpawnPosition(Vector2 spawnPosition)
	{
		return new Vector3(-1.5f + spawnPosition.x * 1.5f, -1.5f + spawnPosition.y * 1.5f);
	}

	private void StartTimerForNextSpawn()
	{
		Invoke("SpawnRandomCard", spawnInterval.Evaluate(Time.timeSinceLevelLoad / ExpectedGameLength) * LongRunSpawnInterval);
	}
}
