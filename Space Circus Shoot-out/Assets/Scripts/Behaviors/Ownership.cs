﻿using UnityEngine;
using System.Collections;

public enum Owner
{
	None,
	Left,
	Right,
}

public class Ownership : MonoBehaviour
{
	public Owner owner;
}
