﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Ownership))]
public class DamageDealer : MonoBehaviour
{
	public float damage = 1;
	public float cooldown = 1;
	public bool destroyOnDealDamage = false;
	public GameObject createOnDestroyByHitPrefab;
	public AudioClip[] dealDamageSounds;

	private bool isOnCooldown = false;
	private bool IsOnCooldown
	{
		get
		{
			return isOnCooldown;
		}
		set
		{
			isOnCooldown = value;

			if (IsOnCooldown)
			{
			    var animator = GetComponent<Animator>();
			    if (animator != null)
			    {
                    animator.SetBool("OnCooldown", true);
			    }
			    CancelInvoke("SetOffCooldown");
				Invoke("SetOffCooldown", cooldown);
			}
		}
	}

	private void SetOffCooldown()
	{
		IsOnCooldown = false;
        var animator = GetComponent<Animator>();
        if (animator != null)
        {
            animator.SetBool("OnCooldown", false);
        }
	}

	void OnTriggerStay2D(Collider2D other)
	{
		var otherOwnership = other.GetComponent<Ownership>();

		if (otherOwnership != null && otherOwnership.owner != GetComponent<Ownership>().owner && !IsOnCooldown)
		{
			var health = other.transform.GetComponent<Health>();
			if (health != null)
			{
				Utility.PlayRandomSound(dealDamageSounds);
				health.Value -= damage;
				var blood = GameObject.Instantiate(Utility.BloodPrefab);
				blood.transform.position = health.transform.position;
				if (destroyOnDealDamage)
				{
					this.CreateDestruction(createOnDestroyByHitPrefab);
					Destroy(gameObject);
				}
				IsOnCooldown = true;
			}
		}
	}
}
