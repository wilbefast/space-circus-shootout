﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Ownership))]
public class Movement : MonoBehaviour
{
	public float startSpeed = 1;

	private float speed;
	public float Speed
	{
		get
		{
			return speed;
		}
		set
		{
			speed = value;

			if (GetComponent<Rigidbody2D>().velocity.magnitude > 0)
			{
				SetVelocity();
			}
		}
	}

	private Owner Owner
	{
		get
		{
			return GetComponent<Ownership>().owner;
		}
	}

	private bool isMoving;
	public bool IsMoving
	{
		get
		{
			return isMoving;
		}
		set
		{
			isMoving = value;

			if (IsMoving)
			{
				SetVelocity();
			}
			else
			{
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				GetComponent<Animator>().SetFloat("Movespeed", 0);

				CancelInvoke("StartMoving");
				Invoke("StartMoving", startMoveDelay);
			}
		}
	}

	private void SetVelocity()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector2(Owner == Owner.Left ? Speed : -Speed, 0);
		GetComponent<Animator>().SetFloat("Movespeed", Speed);
	}

	void Start()
	{
		Speed = startSpeed;
		IsMoving = true;
	}

	private const float startMoveDelay = 1f;

	private void StartMoving()
	{
		IsMoving = true;
	}

	void OnTriggerStay2D(Collider2D other)
	{
		var otherUnit = other.GetComponent<Unit>();
		if (otherUnit != null)
		{
			var ownership = GetComponent<Ownership>();

			if ((ownership.owner == Owner.Left && other.GetComponent<BoxCollider2D>().bounds.max.x > GetComponent<BoxCollider2D>().bounds.max.x) ||
				(ownership.owner == Owner.Right && other.GetComponent<BoxCollider2D>().bounds.min.x < GetComponent<BoxCollider2D>().bounds.min.x))
			{
				IsMoving = false;
			}
		}
	}

	void Update()
	{
		var shooter = GetComponentInChildren<Shooter>();
		if (shooter != null && shooter.IsOnCooldown)
		{
			IsMoving = false;
		}
	}
}
