﻿using UnityEngine;

public class GoToSceneOnClick : MonoBehaviour
{
    public string sceneName;

    void Update()
    {
        if (Time.timeSinceLevelLoad > 0.25f &&
            (Input.touchCount > 0 ||
            Input.GetButtonDown("ClickLeft") ||
            Input.GetButtonDown("ClickRight") ||
            Input.GetMouseButtonDown(0)))
        {
            Application.LoadLevel(sceneName);
        }
    }
}
