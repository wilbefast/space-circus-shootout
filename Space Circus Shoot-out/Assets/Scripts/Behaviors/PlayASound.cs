﻿using UnityEngine;
using System.Collections;

public class PlayASound : MonoBehaviour
{
	public AudioClip sound;

	void Start()
	{
		Utility.PlaySound(sound);
	}
}