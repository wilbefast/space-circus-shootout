﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour
{
	public float time = 1;
	public GameObject createOnDestroyPrefab;
	public AudioClip[] playOnDestroySounds;

	void Start()
	{
		Invoke("Destroy", time);
	}

	private void Destroy()
	{
		Utility.PlayRandomSound(playOnDestroySounds);
		this.CreateDestruction(createOnDestroyPrefab);
		Destroy(gameObject);
	}
}