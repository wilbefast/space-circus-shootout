﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{
	public float startValue = 1;
	public GameObject createOnDeathPrefab;
	public AudioClip[] deathSounds;

	private float value;
	public float Value
	{
		get
		{
			return value;
		}
		set
		{
			this.value = value;

			if (Value <= 0)
			{
				Utility.PlayRandomSound(deathSounds);
				this.CreateDestruction(createOnDeathPrefab);
				Destroy(gameObject);
			}
		}
	}

	void Start()
	{
		Value = startValue;
	}
}
