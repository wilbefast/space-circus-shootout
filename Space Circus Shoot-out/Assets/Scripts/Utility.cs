﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Utility
{
	public static Material LeftColoringMaterial
	{
		get
		{
			return Resources.Load<Material>("ColorReplace");
		}
	}

	public static Material GrayscaleMaterial
	{
		get
		{
			return Resources.Load<Material>("Grayscale");
		}
	}

	public static GameObject BloodPrefab
	{
		get
		{
			return Resources.Load<GameObject>("Blood");
		}
	}

	public static void CreateDestruction(this MonoBehaviour thisObject, GameObject destructionPrefab)
	{
		if (destructionPrefab != null)
		{
			var newObject = GameObject.Instantiate(destructionPrefab);
			newObject.transform.position = thisObject.transform.position;
			var newRenderer = newObject.GetComponent<SpriteRenderer>();
			if (newRenderer != null)
			{
				newRenderer.material = thisObject.GetComponent<SpriteRenderer>().material;
			}

			newObject.transform.localScale = thisObject.transform.localScale;
		}
	}

	public static void Shuffle<T>(this IList<T> list)
	{
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = Random.Range(0, n);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	internal static void PlaySound(AudioClip sound)
	{
		var source = Camera.main.GetComponent<AudioSource>();
		if (source == null)
		{
			source = Camera.main.gameObject.AddComponent<AudioSource>();
		}

		source.PlayOneShot(sound);
	}

	internal static void PlayRandomSound(AudioClip[] sounds)
	{
		if (sounds != null && sounds.Length > 0)
		{
			PlaySound(sounds[Random.Range(0, sounds.Length - 1)]);
		}
	}
}