﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Ownership))]
public class Unit : MonoBehaviour
{
	public float width = 1;

	public AudioClip[] spawnSounds;

	void Awake()
	{
		var rigidbody = gameObject.AddComponent<Rigidbody2D>();
		rigidbody.sleepMode = RigidbodySleepMode2D.NeverSleep;
		rigidbody.isKinematic = true;
	}

	void Start()
	{
		Utility.PlayRandomSound(spawnSounds);

		var ownership = GetComponent<Ownership>();
		if (ownership.owner == Owner.Right)
		{
			transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		}
		else
		{
			GetComponent<SpriteRenderer>().material = Utility.LeftColoringMaterial;
		}

		var collider = gameObject.AddComponent<BoxCollider2D>();
		collider.isTrigger = true;
		collider.size = new Vector2(width, collider.size.y);
	}
}
