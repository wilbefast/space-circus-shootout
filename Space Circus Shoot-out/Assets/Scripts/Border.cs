﻿using UnityEngine;
using System.Collections;

public class Border : MonoBehaviour
{
	public WinScreen winPrefab;

	public Sprite borderNeutral;
	public Sprite borderPushingFromLeft;
	public Sprite borderPushingFromRight;

	private Owner pushedBy;
	private Owner PushedBy
	{
		get
		{
			return pushedBy;
		}
		set
		{
			pushedBy = value;

			switch(PushedBy)
			{
				case Owner.None:
					GetComponent<SpriteRenderer>().sprite = borderNeutral;
					break;

				case Owner.Left:
					GetComponent<SpriteRenderer>().sprite = borderPushingFromLeft;
					break;

				case Owner.Right:
					GetComponent<SpriteRenderer>().sprite = borderPushingFromRight;
					break;
			}

			if (PushedBy != Owner.None)
			{
				const float resetPushedByTime = 1;
				CancelInvoke("ResetPushedBy");
				Invoke("ResetPushedBy", resetPushedByTime);
			}
		}
	}

	private void ResetPushedBy()
	{
		PushedBy = Owner.None;
	}

	void Start()
	{
		ResetPushedBy();
	}

	void OnTriggerStay2D(Collider2D other)
	{
		var otherUnit = other.GetComponent<Unit>();

		if (otherUnit != null)
		{
			var otherOwnership = otherUnit.GetComponent<Ownership>();
			var otherCollider = otherUnit.GetComponent<BoxCollider2D>();
			var collider = GetComponent<BoxCollider2D>();

			var xLeftDistance = otherCollider.bounds.max.x - collider.bounds.max.x;
			var xRightDistance = otherCollider.bounds.min.x - collider.bounds.min.x;

			if (otherOwnership.owner == Owner.Left && xLeftDistance > 0)
			{
				PushedBy = Owner.Left;
				transform.position += new Vector3(xLeftDistance, 0);
			}
			else if (otherOwnership.owner == Owner.Right && xRightDistance < 0)
			{
				PushedBy = Owner.Right;
				transform.position += new Vector3(xRightDistance, 0);
			}

			if (PushedBy != Owner.None)
			{
				const float borderPushMovementFactor = 0.4f;
				var otherMovement = otherUnit.GetComponent<Movement>();
				otherMovement.Speed = otherMovement.startSpeed * borderPushMovementFactor;
			}

			if (GameObject.FindObjectOfType<WinScreen>() == null)
			{
				const float winDistance = 3.5f;
				if (collider.bounds.max.x > winDistance)
				{
					Application.LoadLevel("RedWin");
				}
				else if (collider.bounds.min.x < -winDistance)
				{
					Application.LoadLevel("BlueWin");
				}
			}
		}
	}
}