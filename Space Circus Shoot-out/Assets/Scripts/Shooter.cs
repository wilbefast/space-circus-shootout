﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour
{
	public float cooldown = 1;
	public float range = 5;
	public float bulletSpeed = 1;
	public DamageDealer bulletPrefab;

	public AudioClip[] shootSounds;

	private Owner Owner
	{
		get
		{
			return transform.parent.GetComponent<Ownership>().owner;
		}
	}

	private bool isOnCooldown = false;
	public bool IsOnCooldown
	{
		get
		{
			return isOnCooldown;
		}
		set
		{
			isOnCooldown = value;

			if (IsOnCooldown)
			{
				CancelInvoke("SetOffCooldown");
				Invoke("SetOffCooldown", cooldown);
			}
		}
	}

	private void SetOffCooldown()
	{
		IsOnCooldown = false;
	}

	void Update()
	{
		var collisions = Physics2D.LinecastAll(transform.position, new Vector2(transform.position.x + (Owner == Owner.Left ? range : -range), transform.position.y));
		foreach(var collision in collisions)
		{
			var otherUnit = collision.transform.GetComponent<Unit>();
			if (otherUnit != null)
			{
				var otherOwnership = collision.transform.GetComponent<Ownership>();
				if (otherOwnership.owner != Owner)
				{
					TryToFire();
				}
			}
		}
	}

	private void TryToFire()
	{
		if (!IsOnCooldown)
		{
			Utility.PlayRandomSound(shootSounds);

			var bullet = GameObject.Instantiate<DamageDealer>(bulletPrefab);
			bullet.transform.position = transform.position;
			bullet.GetComponent<Rigidbody2D>().velocity = new Vector3(Owner == Owner.Left ? bulletSpeed : -bulletSpeed, 0);
			bullet.GetComponent<Ownership>().owner = Owner;
			if (Owner == Owner.Left)
			{
				bullet.GetComponentInChildren<SpriteRenderer>().material = Utility.LeftColoringMaterial;
			}
			else
			{
				bullet.transform.localScale = new Vector3(-bullet.transform.localScale.x, bullet.transform.localScale.y, bullet.transform.localScale.z);
			}
			var timer = bullet.gameObject.AddComponent<DestroyAfterTime>();
			timer.time = range / bulletSpeed;
			timer.createOnDestroyPrefab = bullet.GetComponent<DamageDealer>().createOnDestroyByHitPrefab;
			IsOnCooldown = true;
		}
	}
}