﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Ownership))]
public class Stack : MonoBehaviour
{
	public Sprite lockSprite;

	private const float LockdownTime = 3;

	private const float XDistanceToSpawn = 0.25f;
	private const float MaxYDistanceToSpawn = 1f;
	private const float Top = -9;

	public Card cardPrefab;
	public Unit[] unitsInCards;

	private List<Card> Cards { get; set; }

	public Vector3? DragPositionOffset { get; set; }

	private SpriteRenderer SpriteIcon { get; set; }

	void Awake()
	{
		var lockIcon = new GameObject();
		lockIcon.transform.parent = transform;
		lockIcon.transform.localPosition = new Vector3(0, 0, Top - 0.01f);
		SpriteIcon = lockIcon.AddComponent<SpriteRenderer>();
		SpriteIcon.sprite = lockSprite;
	}

	void Start()
	{
		Cards = new List<Card>();
		for (int i = 0; i < unitsInCards.Length; i++)
		{
			var card = GameObject.Instantiate<Card>(cardPrefab);
			card.UnitPrefab = unitsInCards[i];
			card.IsPickup = GetComponent<Ownership>().owner == Owner.None;
			Add(card, lockStack: false);
		}

		IsLockdown = false;
	}

	private bool isLockdown = false;
	public bool IsLockdown
	{
		get
		{
			return isLockdown;
		}
		set
		{
			if (IsLockdown != value)
			{
				foreach (var card in Cards)
				{
					card.SwapGrayscale();
				}
			}

			isLockdown = value;

			if (IsLockdown)
			{
				CancelInvoke("SetOffLockdown");
				Invoke("SetOffLockdown", LockdownTime);
			}

			SpriteIcon.enabled = IsLockdown;
		}
	}

	private void SetOffLockdown()
	{
		IsLockdown = false;
	}

	public void Add(Card card, bool lockStack = true)
	{
		if (GetComponent<Ownership>().owner == Owner.Left)
		{
			card.GetComponent<SpriteRenderer>().material = Utility.LeftColoringMaterial;
		}

		Cards.Insert(0, card);
		RestackCards();

		if (lockStack)
		{
			IsLockdown = true;
		}
	}

	void Update()
	{
		if (Cards.Count > 0)
		{
			var topCard = Cards[0];
			if (DragPositionOffset.HasValue)
			{
				PositionCardTo(topCard, DragPositionOffset.Value);

				var difference = topCard.transform.position - transform.position;
				if (Mathf.Abs(difference.y) < MaxYDistanceToSpawn &&
					((GetComponent<Ownership>().owner == Owner.Left && difference.x > XDistanceToSpawn) ||
					(GetComponent<Ownership>().owner == Owner.Right && difference.x < -XDistanceToSpawn)))
				{
					TryToSpawn();
				}
			}
			else
			{
				RestackCards();
			}
		}
	}

	private void PositionCardTo(Card card, Vector3 offset)
	{
		var newPosition = transform.position + offset;
		newPosition.z = Cursor.ZPosition + 0.01f;
		card.transform.position = newPosition;
	}

	private void RestackCards()
	{
		for(int i = 0; i < Cards.Count; i++)
		{
			var card = Cards[i];
			const float allowedOffset = 0.35f;
			float offset = 0;
			if (i > 0)
			{
				offset = -allowedOffset * (float)i / (Cards.Count - 1);
				if (GetComponent<Ownership>().owner == Owner.Right)
				{
					offset *= -1;
				}
			}
			card.transform.position = new Vector3(transform.position.x + offset, transform.position.y, Top + i * 0.1f);
		}
	}

	public void TryToSpawn()
	{
		if (Cards.Count > 0)
		{
			var cardToSpawn = Cards[0];
			var spawnedUnit = GameObject.Instantiate<Unit>(cardToSpawn.UnitPrefab);
			var spawnOffset = 0.4f + Random.Range(0, 0.01f);
			spawnedUnit.transform.position = new Vector3(transform.position.x + (GetComponent<Ownership>().owner == Owner.Left ? spawnOffset : -spawnOffset), transform.position.y);
			spawnedUnit.GetComponent<Ownership>().owner = GetComponent<Ownership>().owner;
			Destroy(cardToSpawn.gameObject);
			Cards.RemoveAt(0);

			DragPositionOffset = null;
		}
	}

	public void TransferTopCardTo(Stack newStack)
	{
		var topCard = Cards[0];
		Cards.RemoveAt(0);
		newStack.Add(topCard);

		if (Cards.Count == 0 && GetComponent<Ownership>().owner == Owner.None)
		{
			topCard.IsPickup = newStack.GetComponent<Ownership>().owner == Owner.None;
			Destroy(gameObject);
		}
	}
}
