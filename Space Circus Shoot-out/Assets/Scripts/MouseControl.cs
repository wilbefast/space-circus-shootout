﻿using UnityEngine;

[RequireComponent(typeof(Cursor))]
public class MouseControl : MonoBehaviour
{
	void Update()
	{
		var cursor = GetComponent<Cursor>();

		var newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		cursor.SetPosition(newPosition);

		cursor.IsHeld = Input.GetMouseButton(0);
	}
}