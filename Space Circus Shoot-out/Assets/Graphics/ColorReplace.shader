﻿
Shader "Sprites/ColorReplace"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)

		_ColorReplace ("Replace", Color) = (1,1,1,1)
		_ColorReplaceWith ("ReplaceWith", Color) = (1,1,1,1)

		_ColorReplace2 ("Replace2", Color) = (1,1,1,1)
		_ColorReplaceWith2 ("ReplaceWith2", Color) = (1,1,1,1)

		_ColorReplace3 ("Replace3", Color) = (1,1,1,1)
		_ColorReplaceWith3 ("ReplaceWith3", Color) = (1,1,1,1)

		_ColorReplace4 ("Replace4", Color) = (1,1,1,1)
		_ColorReplaceWith4 ("ReplaceWith4", Color) = (1,1,1,1)

		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#pragma target 3.0
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
			};
			
			fixed4 _Color;

			fixed4 _ColorReplace;
			fixed4 _ColorReplaceWith;

			fixed4 _ColorReplace2;
			fixed4 _ColorReplaceWith2;

			fixed4 _ColorReplace3;
			fixed4 _ColorReplaceWith3;

			fixed4 _ColorReplace4;
			fixed4 _ColorReplaceWith4;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color * _Color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;

				float eps = 0.001f;

				if (c.r > _ColorReplace.r - eps && c.r < _ColorReplace.r + eps &&
					c.g > _ColorReplace.r - eps && c.g < _ColorReplace.g + eps &&
					c.b > _ColorReplace.r - eps && c.b < _ColorReplace.b + eps)
				{
					c.rgb = _ColorReplaceWith.rgb;
				}

				if (c.r > _ColorReplace2.r - eps && c.r < _ColorReplace2.r + eps &&
					c.g > _ColorReplace2.r - eps && c.g < _ColorReplace2.g + eps &&
					c.b > _ColorReplace2.r - eps && c.b < _ColorReplace2.b + eps)
				{
					c.rgb = _ColorReplaceWith2.rgb;
				}

				if (c.r > _ColorReplace3.r - eps && c.r < _ColorReplace3.r + eps &&
					c.g > _ColorReplace3.r - eps && c.g < _ColorReplace3.g + eps &&
					c.b > _ColorReplace3.r - eps && c.b < _ColorReplace3.b + eps)
				{
					c.rgb = _ColorReplaceWith3.rgb;
				}

				if (c.r > _ColorReplace4.r - eps && c.r < _ColorReplace4.r + eps &&
					c.g > _ColorReplace4.r - eps && c.g < _ColorReplace4.g + eps &&
					c.b > _ColorReplace4.r - eps && c.b < _ColorReplace4.b + eps)
				{
					c.rgb = _ColorReplaceWith4.rgb;
				}

				c.rgb *= c.a;

				return c;
			}
		ENDCG
		}
	}
}
